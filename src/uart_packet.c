#include "uart.h"
#include "uart_packet.h"
#include "system.h" 

#ifdef UPACKET_USE_CRC16
#include "crc16.h"
#endif

static void Receive(uint8_t uart, uint8_t c, uint8_t * packet);

// determine how many upacket modules are being used
#ifdef USE_UPACKET_7
    #define NUM_UPACKETS 8
#elif defined USE_UPACKET_6
	#define NUM_UPACKETS 7
#elif defined USE_UPACKET_5
	#define NUM_UPACKETS 6
#elif defined USE_UPACKET_4
	#define NUM_UPACKETS 5
#elif defined USE_UPACKET_3
	#define NUM_UPACKETS 4
#elif defined USE_UPACKET_2
	#define NUM_UPACKETS 3
#elif defined USE_UPACKET_1
	#define NUM_UPACKETS 2
#elif defined USE_UPACKET_0
	#define NUM_UPACKETS 1
#else
#warning "No UART Packet modules  used, remove this file from project or define USE_UPACKET_n macro in system.h"
#endif

#ifdef USE_UPACKET_7
    static uint8_t packet7[UPACKET_MAX_LENGTH];
    static void CharReceiver7(char c) { Receive(7, c, &packet7[0]); }
#endif
#ifdef USE_UPACKET_6
    static uint8_t packet6[UPACKET_MAX_LENGTH];
    static void CharReceiver6(char c) { Receive(6, c, &packet6[0]); }
#endif
#ifdef USE_UPACKET_5
    static uint8_t packet5[UPACKET_MAX_LENGTH];
    static void CharReceiver5(char c) { Receive(5, c, &packet5[0]); }
#endif
#ifdef USE_UPACKET_4
    static uint8_t packet4[UPACKET_MAX_LENGTH];
    static void CharReceiver4(char c) { Receive(4, c, &packet4[0]); }
#endif
#ifdef USE_UPACKET_3
    static uint8_t packet3[UPACKET_MAX_LENGTH];
    static void CharReceiver3(char c) { Receive(3, c, &packet3[0]); }
#endif
#ifdef USE_UPACKET_2
    static uint8_t packet2[UPACKET_MAX_LENGTH];
    static void CharReceiver2(char c) { Receive(2, c, &packet2[0]); }
#endif
#ifdef USE_UPACKET_1
    static uint8_t packet1[UPACKET_MAX_LENGTH];
    static void CharReceiver1(char c) { Receive(1, c, &packet1[0]); }
#endif
#ifdef USE_UPACKET_0
    static uint8_t packet0[UPACKET_MAX_LENGTH];
    static void CharReceiver0(char c) { Receive(0, c, &packet0[0]); }
#endif

// array of callbacks
void (*callbacks[NUM_UPACKETS])(uint8_t *, uint16_t );
static uint8_t enabled[NUM_UPACKETS];
void (*dump_callbacks[NUM_UPACKETS])(uint8_t);

void UPacket_Init(uint8_t uart, void (*callback)(uint8_t * data, uint16_t length)) {
    // register receiver for the selected UART channel
    switch(uart) {
#ifdef USE_UPACKET_0
        case 0: UART_RegisterReceiver(0, CharReceiver0); break;
#endif
#ifdef USE_UPACKET_1
        case 1: UART_RegisterReceiver(1, CharReceiver1); break;
#endif
#ifdef USE_UPACKET_2
        case 2: UART_RegisterReceiver(2, CharReceiver2); break;
#endif
#ifdef USE_UPACKET_3
        case 3: UART_RegisterReceiver(3, CharReceiver3); break;
#endif
#ifdef USE_UPACKET_4
        case 4: UART_RegisterReceiver(4, CharReceiver4); break;
#endif
#ifdef USE_UPACKET_5
        case 5: UART_RegisterReceiver(5, CharReceiver5); break;
#endif
#ifdef USE_UPACKET_6
        case 6: UART_RegisterReceiver(6, CharReceiver6); break;
#endif
#ifdef USE_UPACKET_7
        case 7: UART_RegisterReceiver(7, CharReceiver7); break;
#endif
        default: return; // abort if the uart number is invalid
    }
    callbacks[uart] = callback;
    enabled[uart] = 1;
}

void UPacket_Disable(uint8_t uart) {
    switch(uart) {
#ifdef USE_UPACKET_0
        case 0: UART_UnregisterReceiver(0, CharReceiver0); break;
#endif
#ifdef USE_UPACKET_1
        case 1: UART_UnregisterReceiver(1, CharReceiver1); break;
#endif
#ifdef USE_UPACKET_2
        case 2: UART_UnregisterReceiver(2, CharReceiver2); break;
#endif
#ifdef USE_UPACKET_3
        case 3: UART_UnregisterReceiver(3, CharReceiver3); break;
#endif
#ifdef USE_UPACKET_4
        case 4: UART_UnregisterReceiver(4, CharReceiver4); break;
#endif
#ifdef USE_UPACKET_5
        case 5: UART_UnregisterReceiver(5, CharReceiver5); break;
#endif
#ifdef USE_UPACKET_6
        case 6: UART_UnregisterReceiver(6, CharReceiver6); break;
#endif
#ifdef USE_UPACKET_7
        case 7: UART_UnregisterReceiver(7, CharReceiver7); break;
#endif
        default: return; // abort if the uart number is invalid
    }
    enabled[uart] = 0;
}

void UPacket_Enable(uint8_t uart) {
    UPacket_Init(uart, callbacks[uart]);
}

void UPacket_Send(uint8_t uart, uint8_t * data, uint16_t length) {
    if(enabled[uart] == 1) {
    // calculate check
#ifdef UPACKET_USE_CRC16
    crc_t check;
    // seed with the length
    check = length;
    check = CRC16_Update(check, data, length);
    check = CRC16_Finalize(check);
#else
		uint16_t i;
    uint16_t check = length;
    for(i = 0; i < length; i++) check += data[i];
    check = ~check + 1;
#endif
    // send the header
    UART_WriteByte(uart, UPACKET_START_L);
    UART_WriteByte(uart, UPACKET_START_H);
    UART_WriteByte(uart, length&0xFF);
    UART_WriteByte(uart, (length>>8)&0xFF);
    // send the data
    UART_Write(uart, (char *)data, length);
    // send the checksum
    UART_WriteByte(uart, check&0xFF);
    UART_WriteByte(uart, (check>>8)&0xFF);
}
}

void UPacket_RegisterDumpCallback(uint8_t uart, void(*dump)(uint8_t)) {
    dump_callbacks[uart] = dump;
}

#define UPACKET_RECEIVE_STATE_START_L 0
#define UPACKET_RECEIVE_STATE_START_H 1
#define UPACKET_RECEIVE_STATE_LENGTH_L 2
#define UPACKET_RECEIVE_STATE_LENGTH_H 3
#define UPACKET_RECEIVE_STATE_DATA 4
#define UPACKET_RECEIVE_STATE_CHECKSUM_L 5
#define UPACKET_RECEIVE_STATE_CHECKSUM_H 6
static void Receive(uint8_t uart, uint8_t c, uint8_t * packet) {
    static uint8_t state[NUM_UPACKETS];
    static uint16_t length[NUM_UPACKETS];
    static uint16_t index[NUM_UPACKETS];
#ifdef UPACKET_USE_CRC16
    static crc_t crc[NUM_UPACKETS];
#endif  
    static uint16_t check[NUM_UPACKETS];
    
    switch(state[uart]) {
        case UPACKET_RECEIVE_STATE_START_L:
            if(c == UPACKET_START_L) state[uart] = UPACKET_RECEIVE_STATE_START_H;
            // if not the start byte then it may be data for someone else so dump it if a dump callback is registered
            else if(dump_callbacks[uart]) dump_callbacks[uart](c);
            break;
        case UPACKET_RECEIVE_STATE_START_H:
            state[uart] = (c == UPACKET_START_H) ? UPACKET_RECEIVE_STATE_LENGTH_L : UPACKET_RECEIVE_STATE_START_L;
            break;
        case UPACKET_RECEIVE_STATE_LENGTH_L:
            length[uart] = c;
            state[uart] = UPACKET_RECEIVE_STATE_LENGTH_H;
            break;
        case UPACKET_RECEIVE_STATE_LENGTH_H:
            length[uart] |= ((uint16_t)c)<<8;
            // if length is 0 or >MAX then go back to waiting for start
            state[uart] = (length[uart] != 0 && length[uart] <= UPACKET_MAX_LENGTH) ? UPACKET_RECEIVE_STATE_DATA : UPACKET_RECEIVE_STATE_START_L;
            index[uart] = 0;
#ifdef UPACKET_USE_CRC16
            // seed the crc with the length
            crc[uart] = length[uart];
            check[uart] = 0;
#else
            check[uart] = length[uart];
#endif
            break;
        case UPACKET_RECEIVE_STATE_DATA:
            packet[index[uart]++] = c;
            if(index[uart] >= length[uart]) state[uart] = UPACKET_RECEIVE_STATE_CHECKSUM_L;
#ifdef UPACKET_USE_CRC16
            crc[uart] = CRC16_Update(crc[uart], &c, sizeof(uint8_t));
#else
            check[uart] += c;
#endif
            break;
        case UPACKET_RECEIVE_STATE_CHECKSUM_L:
            check[uart] += c;
            state[uart] = UPACKET_RECEIVE_STATE_CHECKSUM_H;
            break;
        case UPACKET_RECEIVE_STATE_CHECKSUM_H:
            check[uart] += ((uint16_t)c)<<8;
            // if the checksum/crc works out then call the callback
#ifdef UPACKET_USE_CRC16
            if(crc[uart] == check[uart]) {
#else
            if(check[uart] == 0) {
#endif
                callbacks[uart](packet, length[uart]);
            }
            // break is omitted intentionally
        default:
            state[uart] = UPACKET_RECEIVE_STATE_START_L;
            break;
    }
}
