#include "watchdog.h"

#define WATCHDOG_PERIOD_IN_MS (WATCHDOG_TIMEOUT_IN_MS/2)

static void WatchdogTask(void);

void Watchdog_Init(void) {
    static uint8_t initialized = 0;
    if(initialized == 0) {
#ifdef _TASK_H_
        Task_Schedule(WatchdogTask, 0, WATCHDOG_PERIOD_IN_MS, WATCHDOG_PERIOD_IN_MS);
#endif
	hal_Watchdog_Enable();
        initialized = 1;
    }
}

uint8_t reset_watchdog = 0;

void Watchdog_Reset(void) {
    reset_watchdog = 1;
}

void WatchdogTask(void) {
    if(reset_watchdog) {
        hal_Watchdog_Clear();
        reset_watchdog = 0;
    }
}
