#ifndef _HAL_GENERAL_
#define _HAL_GENERAL_

#include <stdint.h>
#include <stdbool.h>
//#include "chip.h"

#define Nop() 	__asm("        nop\n");
#define DisableInterrupts() __disable_irq()
#define EnableInterrupts() __enable_irq()

#define BlockInterrupts() __disable_irq()
#define RestoreInterrupts() __enable_irq()

#define hal_Watchdog_Clear() *((uint32_t *)0x400E1450) = 0xA5000001
#define hal_Watchdog_Enable() // must be done in main / startup

#endif // _HAL_GENERAL_
