#include <stdint.h>
#include <xc.h>
#include "eeprom.h"
#include "hal_nvm.h"
#include "timing.h"

uint16_t getPageIdNumber(void* address);
uint16_t getNextPageIdNumber(uint16_t pageId);
void repack(uint16_t oldPage, uint16_t newPage);
static uint16_t pageInUse;
static uint16_t pageIdInUse;
#define PAGES 2
#define DOUBLE_WORDS_PER_PAGE 256
#define BYTES_PER_PAGE DOUBLE_WORDS_PER_PAGE*8
const uint64_t eedata_addr[PAGES][DOUBLE_WORDS_PER_PAGE] __attribute__ ((aligned(2048)))={0};

void hal_EEPROM_Init(void) {
    unsigned int returnCode;
    
    // Delay 40ms to prevent collision with ICSP programming
    // See PIC32MM0128GPM028 Errata section 22
    DelayMs(40);
    
    // Erase any pages that are zeros (typically from programming)
    uint16_t i;
    for (i = 0; i < PAGES; i++) {
        if (eedata_addr[i][0] == 0x0) {
            returnCode = hal_NVM_ErasePage((void*) eedata_addr[i]);
        }
    }
    
    // Check for multiple pages of data existing at once
    unsigned int pagesWithData = 0;
    uint16_t pageIds[PAGES];
    for (i = 0; i < PAGES; i++) {
        pageIds[i] = getPageIdNumber((void*) eedata_addr[i]);
        if ((pageIds[i] < 0xFFFF) && (pageIds[i] > 0x0)) {
            pagesWithData++;
            pageInUse = i;
            pageIdInUse = pageIds[i];
        }
    }
    
    uint16_t pageId, nextPage;
    if (pagesWithData == 1) {
        // Valid page, already set to be in use
        
        // Check if the page is full of data and needs to be repacked
        pageId = getPageIdNumber((void*) &eedata_addr[pageInUse][DOUBLE_WORDS_PER_PAGE - 1]);
        if ((pageId < 0xFFFF) && (pageId > 0x0)) {
            nextPage = pageInUse + 1;
            if (nextPage >= PAGES) {
                nextPage = 0;
            }
            repack(pageInUse, nextPage);
        }
    } else if (pagesWithData > 1) {
        // This indicates a reset occurred while it was packing and it should repack
        // TODO If this will support more than two pages, change this algorithm
        // to find the two most recent pages (?)
#if PAGES > 2
#error "More than two pages not supported yet"
#endif
        uint16_t old, new;
        if (pageIds[0] == 1) {
            if (pageIds[1] == 2) {
                old = 0;
                new = 1;
            } else if (pageIds[1] == 3) {
                old = 1;
                new = 0;
            }
        } else if (pageIds[0] == 2) {
            if (pageIds[1] == 1) {
                old = 1;
                new = 0;
            } else if (pageIds[1] == 3) {
                old = 0;
                new = 1;
            }
        } else if (pageIds[0] == 3) {
            if (pageIds[1] == 1) {
                old = 0;
                new = 1;
            } else if (pageIds[1] == 2) {
                old = 1;
                new = 0;
            }
        }
        
        repack(old, new);
    } else {
        // No pages detected, use page 0 by default
        pageInUse = 0;
        pageIdInUse = 1;
    }
}

uint16_t hal_EEPROM_Read(uint16_t address) {
    unsigned int data = 0;
    
    uint16_t i, id, addr;
    uint64_t raw;
    for (i = 0; i < DOUBLE_WORDS_PER_PAGE; i++) {
        raw = eedata_addr[pageInUse][i];
        id = raw >> 48;
        if ((id < 0x4) && id > 0x0) {
            // Valid to check against
            addr = (raw >> 32) & 0xFFFF;
            if (address == addr) {
                data = raw & 0xFFFF;
            }
        } else {
            // Done searching
            break;
        }
    }
    
    return data;
}

void hal_EEPROM_Write(uint16_t address, uint16_t data) {
    uint8_t lastMatched = 0;
    uint16_t i, id, ad, da;
    uint64_t raw;
    for (i = 0; i < DOUBLE_WORDS_PER_PAGE; i++) {
        raw = eedata_addr[pageInUse][i];
        id = raw >> 48;
        if ((id < 0xFFFF) && (id > 0x0)) {
            // Valid to check against to prevent a rewrite of existing data
            ad = (raw >> 32) & 0xFFFF;
            da = raw & 0xFFFF;
            if ((address == ad) && (data == da)) {
                lastMatched = 1;
            } else if (address == ad) {
                lastMatched = 0;
            }
        } else if (!lastMatched) {
            // No data matched, add this data
            uint32_t new_h = (pageIdInUse << 16) | address;
            uint32_t new_l = data;
            hal_NVM_WriteDoubleWord((void*) &eedata_addr[pageInUse][i], new_h, new_l);
            break;
        }
    }
    
    // Repack the data if it just reached the end of the page
    uint16_t newPage;
    if (i >= (DOUBLE_WORDS_PER_PAGE - 1)) {
        newPage = pageInUse + 1;
        if (newPage >= PAGES) {
            newPage = 0;
        }
        repack(pageInUse, newPage);
    }
}

uint16_t getPageIdNumber(void* address) {
    uint64_t data = *(uint64_t*)address;
    return data >> 48;
}

uint16_t getNextPageIdNumber(uint16_t pageId) {
    pageId++;
    if (pageId > 3) {
        pageId = 1;
    }
    
    return pageId;
}

void repack(uint16_t oldPage, uint16_t newPage) {
    // Erase the page this will write to
    hal_NVM_ErasePage((void*)eedata_addr[newPage]);
    
    // Storage of the packed data
    uint16_t totalPacked = 0;
    uint16_t packed[2][DOUBLE_WORDS_PER_PAGE];
    
    // Get the data and pack it
    uint16_t i, j;
    uint8_t found;
    for (i = 0; i < DOUBLE_WORDS_PER_PAGE; i++) {
        uint64_t raw = eedata_addr[oldPage][i];
        uint16_t addressRaw = (raw >> 32) & 0xFFFF;
        uint16_t dataRaw = raw & 0xFFFF;
        
        found = 0;
        for (j = 0; j < totalPacked; j++) {
            if (packed[0][j] == addressRaw) {
                packed[1][j] = dataRaw;
                found = 1;
                break;
            }
        }
        
        // If it didn't find it, add it
        if (!found) {
            packed[0][totalPacked] = addressRaw;
            packed[1][totalPacked] = dataRaw;
            totalPacked++;
        }
    }
    
    uint16_t oldId = getPageIdNumber((void*) eedata_addr[oldPage]);
    uint16_t newId = getNextPageIdNumber(oldId);
    for (i = 0; i < totalPacked; i++) {
        uint32_t toWrite_l = packed[1][i];
        uint32_t toWrite_h = (newId << 16) | packed[0][i];
        hal_NVM_WriteDoubleWord((void*) &eedata_addr[newPage][i], toWrite_h, toWrite_l);
    }
    
    pageInUse = newPage;
    pageIdInUse = newId;
    
    // Erase the old page
    hal_NVM_ErasePage((void*) eedata_addr[oldPage]);
}
