#include "hal_nvm.h"
#include <xc.h>

uint32_t hal_NVM_VirtualToPhysicalAddress(uint32_t virtual) {
    return virtual & 0x1FFFFFFF;
}

uint16_t hal_NVM_Unlock(uint16_t nvmop) {
    // Disable interrupts
    __builtin_disable_interrupts();
    
    // Enable flash operations and select the operation to perform
    NVMCON = nvmop;
    
    // Write the unlock keys
    NVMKEY = 0xAA996655;
    NVMKEY = 0x556699AA;
    
    // Start the flash operation
    NVMCONbits.WR = 1;
    
    // Wait for the flash operation to complete
    while (NVMCONbits.WR);
    
    // Restore interrupts
    __builtin_enable_interrupts();
    
    // Disable the flash write enable
    NVMKEY = 0;
    NVMCONbits.WREN = 0;
    
    // Return the status bits
    return (NVMCON & 0x3000);
}

uint16_t hal_NVM_WriteDoubleWord(void* address, uint32_t data_h, uint32_t data_l) {
    uint16_t result;
    
    // Load the data
    NVMDATA0 = data_l;
    NVMDATA1 = data_h;
    
    // Load the address
    NVMADDR = hal_NVM_VirtualToPhysicalAddress((uint32_t) address);
    
    // Unlock and write the double word
    result = hal_NVM_Unlock(0x4002);
    
    // Return the result of the write
    return result;
}

uint16_t hal_NVM_ErasePage(void* address) {
    uint16_t result;
    
    // Load the address of the page to erase
    NVMADDR = hal_NVM_VirtualToPhysicalAddress((uint32_t) address);
    
    // Unlock and erase the page
    result = hal_NVM_Unlock(0x4004);
    
    // Return the result of the erase
    return result;
}
