/**
 * @defgroup hal_nvm HAL NVM Module
 * @ingroup nvm
 * @file hal_nvm.h
 *
 * @{
 */

#ifndef _HAL_NVM_H_
#define _HAL_NVM_H_

#include <stdint.h>

uint32_t hal_NVM_VirtualToPhysicalAddress(uint32_t virtual);
uint16_t hal_NVM_Unlock(uint16_t nvmop);
uint16_t hal_NVM_WriteDoubleWord(void* address, uint32_t data_h, uint32_t data_l);
uint16_t hal_NVM_ErasePage(void* address);

/** @}*/
#endif /* _HAL_NVM_H_ */
