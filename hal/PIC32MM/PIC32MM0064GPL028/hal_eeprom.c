#include "hal_general.h"
#include "eeprom.h"
#include "int_def.h"

/** 
 * This is a very simple EEPROM implementation which works on the PIC32MM.
 * The PIC32MM differs from other PIC32's in the following ways:
 * - Page size is 2048 not 4096
 * - Smallest write is 8 bytes
 * - Can't write same location twice without erasing first due to ECC
 * 
 * For the above reasons the virtual EEPROM provided by microchip won't work.
 * This implementation uses the upper 4 bytes for address and lower 4 for data.
 * Only one page is used which limits max size to 256 words. However, it is
 * intended to be used with relatively small address spaces (EEPROM_SIZE) as
 * it only uses one page. When the page fills up it will back up the contents
 * to RAM then erase then write. If power is lost during this process data loss
 * will occur.
 * 
 * This could be extended to use multiple pages and to make the first address
 * (or unused bytes) a page index to keep track of which page is most recent.
 */

unsigned int NVMWriteDoubleWord(void * address, uint32_t data0, uint32_t data1);
unsigned int NVMErasePage(void* address);
unsigned int NVMUnlock (unsigned int nvmop);

uint16_t ram_eedata[EEPROM_SIZE];

// there are 256 64-bit words per block of 2048 bytes
const uint64_t eedata[256] __attribute__ ((aligned(2048)))={0xFFFFFFFFFFFFFFFF};

void hal_EEPROM_Init(void) {
    // no initialization is required
}

uint16_t hal_EEPROM_Read(uint16_t address) {
    unsigned int data;
    unsigned int i;
    union64_t u64;
    // search for address
    for(i = 255; i >= 0; i--) {
        u64.quad_word = eedata[i];
        if(u64.word[3] == address) {
            return u64.word[0];
        }
    }
    // not found, return 0
    return 0;
}

void hal_EEPROM_Write(uint16_t address, uint16_t data) {
    // find the first empty location
    unsigned int i;
    union64_t u64;
    // return if the data hasn't changed
    if(hal_EEPROM_Read(address) == data) return;
    for(i = 0; i < 256; i++) {
        u64.quad_word = eedata[i];
        if(u64.quad_word == 0xFFFFFFFFFFFFFFFFULL) {
            u64.word[3] = address;
            u64.word[0] = data;
            NVMWriteDoubleWord((void*)&eedata[i], u64.double_word[0], u64.double_word[1]);
            return;
        }
    }
    // if we got here then we couldn't find a spot to write to
    // copy eeprom data to ram_eedata
    for(i = 0; i < EEPROM_SIZE; i++) {
        ram_eedata[i] = hal_EEPROM_Read(i);
    }
    ram_eedata[address] = data;
    // erase the page
    NVMErasePage((void*)eedata);
    // write the eeprom
    for(i = 0; i < EEPROM_SIZE; i++) {
        // if value is non-zero then write it to flash
        if(ram_eedata[i] != 0) {
            hal_EEPROM_Write(i, ram_eedata[i]);
        }
    }
}

unsigned int NVMUnlock (unsigned int nvmop) {
    unsigned int status;
    // Suspend or Disable all Interrupts
    asm volatile ("di %0" : "=r" (status));
    // Enable Flash Write/Erase Operations and Select
    // Flash operation to perform
    NVMCON = nvmop;
    // Write Keys
    NVMKEY = 0xAA996655;
    NVMKEY = 0x556699AA;
    // Start the operation using the Set Register
    NVMCONSET = 0x8000;
    // Wait for operation to complete
    while (NVMCON & 0x8000);
    // Restore Interrupts
    if (status & 0x00000001) {
        asm volatile ("ei");
    } else {
        asm volatile ("di");
    }
    // Disable NVM write enable
    NVMCONCLR = 0x0004000;
    // Return WRERR and LVDERR Error Status Bits
    return (NVMCON & 0x3000);
}

unsigned int NVMErasePage(void* address) {
    unsigned int res;
    // Set NVMADDR to the Start Address of page to erase
    // convert to physical if needed
    NVMADDR = ((unsigned long) address) & 0x1FFFFFFF; 
    // Unlock and Erase Page
    res = NVMUnlock(0x4004);
    // Return Result
    return res;
}

unsigned int NVMWriteDoubleWord(void * address, uint32_t data0, uint32_t data1) {
    unsigned int result;
    NVMDATA0 = data0;
    NVMDATA1 = data1;
    // convert to physical if needed
    NVMADDR = ((unsigned long) address) & 0x1FFFFFFF; 
    result = NVMUnlock (0x4002);                  // 0100 0000 0000 0010
    return result;
}
