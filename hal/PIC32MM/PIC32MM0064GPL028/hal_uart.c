#include <stdint.h>
#include "hal_general.h"
#include "uart.h"
#include "system.h"
#include "macros.h"

#include "hal_uart.h"

#define Set_U2Tx_PPS2(pin, rpor) CAT5(RPOR, rpor, bits.RP, pin, R) = 1 // U2TX
#define Set_U2Tx_PPS(pin) Set_U2Tx_PPS2(pin, CAT2(RPOR_RP,pin))

#define Set_U2Rx_PPS(pin) RPINR9bits.U2RXR = pin

// defaults for PPS if not set in system.h
#ifndef UART2_TX_PIN
#define UART2_TX_PIN 1
#endif
#ifndef UART2_RX_PIN
#define UART2_RX_PIN 2
#endif

#ifndef UART_INTERRUPT_PRIORITY
#define UART_INTERRUPT_PRIORITY 3
#endif

#ifndef UART_INTERRUPT_SUBPRIORITY
#define UART_INTERRUPT_SUBPRIORITY 0
#endif

#ifndef PERIPHERAL_CLOCK
#define PERIPHERAL_CLOCK FCPU
#endif

// private function, could be changed to public if needed
static void SetBaud(uint8_t n, uint32_t baud);

void hal_UART_Init(uint8_t channel, uint32_t baud) {
    hal_UART_Disable(channel);
    SetBaud(channel, baud);
    switch (channel) {
        case UART1_CH:
            IPC6bits.U1EIP = UART_INTERRUPT_PRIORITY; // UERI: UART1 Error
            IPC6bits.U1EIS = UART_INTERRUPT_SUBPRIORITY; // Sub Priority: 0
            IPC6bits.U1TXIP = UART_INTERRUPT_PRIORITY; // UTXI: UART1 Transmission
            IPC6bits.U1TXIS = UART_INTERRUPT_SUBPRIORITY; // Sub Priority
            IPC5bits.U1RXIP = UART_INTERRUPT_PRIORITY; // URXI: UART1 Reception
            IPC5bits.U1RXIS = UART_INTERRUPT_SUBPRIORITY; // Sub Priority

            // interrupt when transmit buffer is empty, enable RX and TX, rx interrupt
            // when not empty
            //        FEDCBA9876543210
            U1STA = 0b1001010000000000; // 0x0
            break;
        case UART2_CH:
            
            Set_U2Tx_PPS(UART2_TX_PIN); // UART2_TX_PIN should be defined in system.h
            // e.g. #define UART2_TX_PIN PRB10
            // Options are RP: D3 G7 F4 D11 F0 B1 E5 C13 B3 F3
            Set_U2Rx_PPS(UART2_RX_PIN); // UART2_RX_PIN should be defined in system.h
            // Options are RP: D2 G8 F4 D10 F1 B9 B10 C14 B5 F2
            
            IPC10bits.U2EIP = UART_INTERRUPT_PRIORITY; // UERI: UART2 Error
            IPC10bits.U2EIS = UART_INTERRUPT_SUBPRIORITY; // Sub Priority: 0
            IPC10bits.U2TXIP = UART_INTERRUPT_PRIORITY; // UTXI: UART2 Transmission
            IPC10bits.U2TXIS = UART_INTERRUPT_SUBPRIORITY; // Sub Priority
            IPC10bits.U2RXIP = UART_INTERRUPT_PRIORITY; // URXI: UART2 Reception
            IPC10bits.U2RXIS = UART_INTERRUPT_SUBPRIORITY; // Sub Priority
            
            // interrupt when transmit buffer is empty, enable RX and TX, rx interrupt
            // when not empty
            //        FEDCBA9876543210
            U2STA = 0b1001010000000000;
            break;
        default:
            return;
    }

    hal_UART_EnableRxInterrupt(channel);
    hal_UART_Enable(channel);
    hal_UART_ClearRxIF(channel);
}

void hal_UART_Enable(uint8_t channel) {
    // NOTE: Make sure to set LAT bit corresponding to TxPin as high before UART initialization!
    switch (channel) {
        case UART1_CH:
            U1MODEbits.ON = 1;
            break;
        case UART2_CH:
            U2MODEbits.ON = 1;
            break;
        default: 
            return;
    }
}

void hal_UART_Disable(uint8_t channel) {
    switch (channel) {
        case UART1_CH: 
            U1MODEbits.ON = 0;
            break; 
        case UART2_CH: 
            U2MODEbits.ON = 0;
            break;
        default: 
            return;
    }
}

void hal_UART_EnableRxInterrupt(uint8_t channel) {
    switch (channel) {
        case UART1_CH:
            IEC0SET = _IEC0_U1RXIE_MASK;
            break;
        case UART2_CH: 
            IEC1SET = _IEC1_U2RXIE_MASK;
            break;
        default: 
            return;
    }
}

void hal_UART_EnableTxInterrupt(uint8_t channel) {
    switch (channel) {
        case UART1_CH: 
            IEC0SET = _IEC0_U1TXIE_MASK;
            break;
        case UART2_CH: 
            IEC1SET = _IEC1_U2TXIE_MASK;
            break;
        default: 
            return;
    }
}

void hal_UART_DisableRxInterrupt(uint8_t channel) {
    switch (channel) {
        case UART1_CH: 
            IEC0CLR = _IEC0_U1RXIE_MASK;
            break;
        case UART2_CH: 
            IEC1CLR = _IEC1_U2RXIE_MASK;
            break;
        default: 
            return;
    }
}

void hal_UART_DisableTxInterrupt(uint8_t channel) {
    switch (channel) {
        case UART1_CH: 
            IEC0CLR = _IEC0_U1TXIE_MASK;
            break;
        case UART2_CH: 
            IEC1CLR = _IEC1_U2TXIE_MASK;
            break;
        default: 
            return;
    }
}

void hal_UART_TxChar(uint8_t channel, char c) {
    switch (channel) {
        case UART1_CH: 
            U1TXREG = c;
            break;
        case UART2_CH: 
            U2TXREG = c;
            break;
        default: 
            return;
    }
}

char hal_UART_RxChar(uint8_t channel) {
    switch (channel) {
        case UART1_CH:
            return U1RXREG;
        case UART2_CH:
            return U2RXREG;
        default:
            return 0;
    }
}

void hal_UART_ClearTxIF(uint8_t channel) {
    switch (channel) {
        case UART1_CH: 
            IFS0CLR = _IFS0_U1TXIF_MASK;
            break;
        case UART2_CH: 
            IFS1CLR = _IFS1_U2TXIF_MASK;
            break;
        default: 
			return;
    }
}

void hal_UART_ClearRxIF(uint8_t channel) {
    switch (channel) {
        case UART1_CH: 
            IFS0CLR = _IFS0_U1RXIF_MASK;
            break;
        case UART2_CH: 
            IFS1CLR = _IFS1_U2RXIF_MASK;
            break;
        default:
			return;
    }
}

uint8_t hal_UART_DataAvailable(uint8_t channel) {
    switch (channel) {
        case UART1_CH: 
            return U1STAbits.URXDA;
        case UART2_CH: 
            return U2STAbits.URXDA;
        default: 
            return 0;
    }
}

uint8_t hal_UART_SpaceAvailable(uint8_t channel) {
    switch (channel) {
        case UART1_CH: 
            return !U1STAbits.UTXBF;
        case UART2_CH: 
            return !U2STAbits.UTXBF;
        default: 
            return 0;
    }
}

// non typical function

uint8_t hal_UART_DoneTransmitting(uint8_t channel) {
    switch (channel) {
        case UART1_CH: 
            return U1STAbits.TRMT;
        case UART2_CH: 
            return U2STAbits.TRMT;
        default: 
            return 0;
    }
}

void SetBaud(uint8_t n, uint32_t baud) {
    int16_t error, error1, error2;
    uint16_t brg, brg1, brg2;
    uint32_t baud2, baud1;
    uint8_t brgh;
    brg1 = PERIPHERAL_CLOCK / (16 * baud) - 1;
    brg2 = brg1 + 1;
    baud1 = PERIPHERAL_CLOCK / (16 * (brg1 + 1));
    baud2 = PERIPHERAL_CLOCK / (16 * (brg2 + 1));
    if (baud1 > baud) error1 = baud1 - baud;
    else error1 = baud - baud1;
    if (baud2 > baud) error2 = baud2 - baud;
    else error2 = baud - baud2;
    if (error1 < error2) {
        error = error1;
        brg = brg1;
    } else {
        error = error2;
        brg = brg2;
    }
    if (error * 1000 / baud > 25) {
        brg1 = PERIPHERAL_CLOCK / (4 * baud) - 1;
        brg2 = brg1 + 1;
        baud1 = PERIPHERAL_CLOCK / (4 * (brg1 + 1));
        baud2 = PERIPHERAL_CLOCK / (4 * (brg2 + 1));
        if (baud1 > baud) error1 = baud1 - baud;
        else error1 = baud - baud1;
        if (baud2 > baud) error2 = baud2 - baud;
        else error2 = baud - baud2;
        if (error1 < error2) {
            brg = brg1;
        } else {
            brg = brg2;
        }
        brgh = 1;
    } else {
        brgh = 0;
    }

    switch (n) {
        case UART1_CH:
            U1MODEbits.BRGH = brgh;
            U1BRG = brg;
            break;
        case UART2_CH:
            U2MODEbits.BRGH = brgh;
            U2BRG = brg;
            break;
        default:
            break;
    }
}

uint8_t hal_UART_RxInterruptEnabled(uint8_t channel) {
    switch (channel) {
        case UART1_CH: 
            return IEC0bits.U1RXIE;
        case UART2_CH: 
            return IEC1bits.U2RXIE;
        default: 
            return 0;
    }
}

uint8_t hal_UART_TxInterruptEnabled(uint8_t channel) {
    switch (channel) {
        case UART1_CH: 
            return IEC0bits.U1TXIE;
        case UART2_CH: 
            return IEC1bits.U2TXIE;
        default: 
            return 0;
    }
}

void __attribute__((vector(_UART1_RX_VECTOR), interrupt(), nomips16)) _UART1_RX_ISR(void) {
    UART_Rx_Handler(UART1_CH);
    hal_UART_ClearRxIF(UART1_CH);
    if (U1STAbits.OERR) {
        UART_Error_Handler(UART1_CH, OVERRUN_ERROR);
        U1STAbits.OERR = 0;
    }
}

void __attribute__((vector(_UART1_TX_VECTOR), interrupt(), nomips16)) _UART1_TX_ISR(void) {
    UART_Tx_Handler(UART1_CH);
    hal_UART_ClearTxIF(UART1_CH);
}

void __attribute__((vector(_UART2_RX_VECTOR), interrupt(), nomips16)) _UART2_RX_ISR(void) {
    UART_Rx_Handler(UART2_CH);
    hal_UART_ClearRxIF(UART2_CH);
    if (U2STAbits.OERR) {
        UART_Error_Handler(UART2_CH, OVERRUN_ERROR);
        U2STAbits.OERR = 0;
    }
}

void __attribute__((vector(_UART2_TX_VECTOR), interrupt(), nomips16)) _UART2_TX_ISR(void) {
    UART_Tx_Handler(UART2_CH);
    hal_UART_ClearTxIF(UART2_CH);
}
