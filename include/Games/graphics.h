#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#include <stdint.h>
/** hal_graphics.h and hal_graphics.c includes any processor specific code 
 * for the display driver. If there is no processor specific code a hal_graphics.h
 * should be created with just a definition for hal_graphics_t.
 * 
 * graphics.c should be a display specific module that is processor independent.
 * 
 */
#include "hal_graphics.h"

/** screen sizes where 0,0 is the upper left */
enum screen_size_e {
    SCREEN_SIZE_128X64,
    SCREEN_SIZE_320X240,
    SCREEN_SIZE_160X128
};

typedef struct {
    // members TBD
    uint8_t foreground[3];
    uint8_t background[3];
    enum screen_size_e screen_size;
    hal_graphics_t hal; /**< hal_graphics_t can hold any display specific data.
                         * if not requried it shoudl be defines as uint8_t in
                         * hal_graphics.h */
} graphics_t;

typedef struct {
    uint16_t x;
    uint16_t y;
} g_point_t;

typedef union {
    uint8_t all;
    struct {
        uint8_t red:2;
        uint8_t green:2;
        uint8_t blue:2;
        uint8_t unused:1;
        uint8_t transparent:1;
    };
} g_pixel_t;

void Graphics_Init(graphics_t * gptr);

/** The output screen size will be static (unchanging for a given graphics.c)
 * but the input screen size will vary depending on the game. This method allow
 * the game to tell the graphics module which screen size it will provide 
 * x,y data in.
 * 
 * @param gptr
 * @param screen_size
 */
void Graphics_SetInputScreenSize(graphics_t * gptr, enum screen_size_e screen_size);

/**
 * Note in the implementation of DrawTile the tile data can be accessed using
 * tile[i][j] where i is the row and j the column.
 * 
 * Note2 it is recommended that tiles for SCREEN_SIZE_320X240 be scaled up by
 * 2x so each "pixel" of the time is 4 pixels on the screen. Alternatively game
 * designers using SCREEN_SIZE_320X240 could provide an alternate tile library
 * optimized for smaller screens.
 * 
 * @param gptr
 * @param position Top left corner of the tile
 * @param tilw 2-D tile array of g_pixel_t
 * @param x width of the tile
 * @param y height of the tile
 */
void Graphics_DrawTile(graphics_t * gptr, g_point_t position, g_pixel_t *tile[], char x, char y);

void Graphics_SetBackground(graphics_t * gptr, uint8_t color[3]);

void Graphics_SetForeground(graphics_t * gptr, uint8_t color[3]);

void Graphics_DrawLine(graphics_t * gptr, g_point_t p1, g_point_t p2);

void Graphics_DrawPixel(graphics_t * gptr, g_point_t p);

void Graphics_DrawRectangle(graphics_t * gptr, g_point_t top_left, g_point_t bottom_right);

/** 
 * It is recommended to implement this function to draw the smallest readable
 * text for the target display.
 * 
 * @param gptr
 * @param position top left corner of the text
 * @param str text string (using printf format)
 * @param ... optional replacement data
 */
void Graphics_DrawText(graphics_t * gptr, g_point_t position, char * str, ...);

void Graphics_ClearScreen(graphics_t * gptr);

void Graphics_UpdateScreen(graphics_t * gptr);

#endif // _GRAPHICS_H_
