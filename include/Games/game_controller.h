#ifndef _GAME_CONTROLLER_H_
#define _GAME_CONTROLLER_H_

#include <stdint.h>

/** @brief Game Controller Module
 *
 * @defgroup game_controller Game Controller Module
 *
 * @ingroup game Game Management / Helper Module
 *
 * This module holds the common information between the @ref game_controller_client
 * and @ref game_controller_host modules.
 * 
 */

/** Primary button union structure to provide simple bit level access */
typedef union {
    uint8_t all_buttons;
    struct {
        uint8_t up:1;
        uint8_t down:1;
        uint8_t left:1;
        uint8_t right:1;
        uint8_t A:1;
        uint8_t B:1;
        uint8_t start:1;
        uint8_t select:1;
    } button;
} controller_primary_buttons_t;

/** All button union structure to provide simple bit level access */
typedef union {
    uint16_t all_buttons;
    uint8_t primary_buttons;
    struct {
        uint16_t up:1;
        uint16_t down:1;
        uint16_t left:1;
        uint16_t right:1;
        uint16_t A:1;
        uint16_t B:1;
        uint16_t start:1;
        uint16_t select:1;
        uint16_t b1:1;
        uint16_t b2:1;
        uint16_t b3:1;
        uint16_t b4:1;
        uint16_t b5:1;
        uint16_t b6:1;
        uint16_t b7:1;
        uint16_t b8:1;
    } button;
} controller_buttons_t;

/** Button id enumeration */
enum e_button_id {
    GAME_BUTTON_UP,
    GAME_BUTTON_DOWN,
    GAME_BUTTON_LEFT,
    GAME_BUTTON_RIGHT,
    GAME_BUTTON_A,
    GAME_BUTTON_B,
    GAME_BUTTON_START,
    GAME_BUTTON_SELECT,
    GAME_BUTTON_B1,
    GAME_BUTTON_B2,
    GAME_BUTTON_B3,
    GAME_BUTTON_B4,
    GAME_BUTTON_B5,
    GAME_BUTTON_B6,
    GAME_BUTTON_B7,
    GAME_BUTTON_B8
};

// Addresses for messages
#define CADDR_BROADCAST 0xE0 	// If the host sends to this address the packet will be acknowledged by all controllers
#define CADDR_NONE 0xC0			// If an address has not been assigned, use this one
#define CADDR_MASK 0xE0			// Mask for getting just the address bits from a header
#define CADDR_OFFSET 5			// Bit shift required to parse address into an int from 0 - MAX_NUM_CONTROLLERS-1

// Message types - 5 bits of data available for message type
#define CMSG_GETADDR	0x00
#define CMSG_SETADDR 	0x01
#define	CMSG_BUTTON		0x02
#define CMSG_SETPERIOD	0x03
// 0x04-0x0F Reserved
// 0x10-0x1F Implementation Specific
#define CMSG_MASK		0x1F	// Mask for getting just the message type from a header

#endif
