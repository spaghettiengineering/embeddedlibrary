#ifndef _GAME_CONTROLLER_HOST_H_
#define _GAME_CONTROLLER_HOST_H_

// include system.h for definition of GAME_CONTROLLER_UART
#include "system.h"
#include "game_controller.h"

/** @brief Game Controller Host Module
 *
 * @defgroup game_controller_client Game Controller Client Module
 *
 * @ingroup game_controller
 *
 * This module is intended to be used on the MCU running a game and connected
 * to a MCU using @ref game_controller_client to implement a game controller.
 * 
 * Note: use "$sys echo" from terminal to echo characters and "$sys GCtlr unmute"
 * from terminal to enable log messages for this module
 */

#ifndef GAME_CONTROLLER_UART
#warning "define GAME_CONTROLLER_UART in system.h"
#endif

/** Typedef for callback type. Useful for casting your actual function to this
 * type to suppress compiler warnings.
 */
typedef void(*gctlr_callback_t)(controller_buttons_t,void*);

/** @brief Initialize the game controller host module
 * 
 * This will register the callback with the UART packet module, initialize
 * the list of button callbacks, and optionally register the GameControllerHost
 * with the Subsystem module (if USE_MODULE_SUBSYS is defined in system.h).
 */
void GameControllerHost_Init(void);

/** @brief add a callback to be called when one or more buttons are pressed
 * 
 * Note: the callback function passed in will be called with button data with
 * 1's set wherever a button was pressed. This should not be confused with
 * the button state as other buttons may be pressed. If the user needs the 
 * button state when processing their callback use 
 * GameControllerHost_GetButtonState().
 * 
 * @param address address of player of interest (0-3) or all players (7)
 * @param callback function pointer to callback to be called when the button is pressed
 * @param mask mask for buttons, callback will only be called when the button pressed is also in the mask
 * @param handle a void * handle that the callback function can optionally use to keep track of their data
 */
void GameControllerHost_RegisterPressCallback(uint8_t address, 
        void(*callback)(controller_buttons_t,void*),
        controller_buttons_t mask, void * handle);

/** @brief add a callback to be called when one or more buttons change state
 * 
 * Note: the callback function passed in will be called with button data with
 * 1's set wherever a button changed state. This should not be confused with
 * the button state. If the user needs the button state when processing their
 * callback use GameControllerHost_GetButtonState().
 * 
 * @param address address of player of interest (0-3) or all players (7)
 * @param callback function pointer to callback to be called when the button changes
 * @param mask mask for buttons, callback will only be called when the changed button is also in the mask
 * @param handle a void * handle that the callback function can optionally use to keep track of their data
 */
void GameControllerHost_RegisterCallback(uint8_t address, 
        void(*callback)(controller_buttons_t,void*),
        controller_buttons_t mask, void * handle);

/** @brief Command the GameControllerClient to send periodic button messages
 * 
 * The GameControllerHost will send a button message whenever a button changes
 * AND whenever this period elapses.
 * 
 * @param period millisecond period, use 0 to disable periodic button messages.
 */
void GameControllerHost_SetPeriod(uint16_t period);

/** @brief Get button states for a given player
 * 
 * Since the callbacks are called with information about which buttons have been
 * pressed or which buttons have changed it may be required to know what the 
 * button state is or what the state of other buttons are. This method can be 
 * called to get the current state of all buttons.
 * 
 * @param address player address (0-3)
 * @return Button data
 */
controller_buttons_t GameControllerHost_GetButtonState(uint8_t address);

/** @brief Remove button callback that were previously added
 * 
 * Both the callback and the handle must match for the callback to be removed.
 * If multiple callbacks used the same handle then all matches will be removed.
 * 
 * @param callback function pointer to callback to be removed
 * @param handle optional handle that was used when the callback was added
 */
void GameControllerHost_RemoveCallback(void(*callback)(controller_buttons_t,void*), 
        void * handle);

/** @brief enable terminal emulation of a controller
 *
 * - w/W UP
 * - s/S DOWN
 * - a/A LEFT
 * - d/D RIGHT
 * - space A
 * - b/B B
 * - enter START
 * - = SELECT
 *
 * @param UART channel to register character receiver to for terminal controller
 * */
void GameControllerHost_EnableTerminalController(uint8_t uart_channel);
void GameControllerHost_DisableTerminalController(uint8_t uart_channel);

#endif
