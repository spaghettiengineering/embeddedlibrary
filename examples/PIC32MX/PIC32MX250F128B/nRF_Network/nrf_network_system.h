/*
 * system.h
 *
 *  Created on: Feb 7, 2015
 *      Author: Anthony
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#define FCPU 40000000
#define PERIPHERAL_CLOCK FCPU

#define USE_SPI1
#define SPI_MAX_SIZE 33

#define USE_UART2
#define SUBSYS_UART 2

#define THIS_NODE MERLINO
#define THIS_NODE2 MASTER

#include "library.h"
#include "task.h"
#include "timing.h"
#include "spi.h"
#include "uart.h"
#include "buffer.h"
#include "buffer_printf.h"
#include "list.h"
#include "nrf24.h"
#include "nrf24network.h"
#include "subsys.h"

#endif /* SYSTEM_H_ */
